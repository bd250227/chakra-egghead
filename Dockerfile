FROM node:16

# create scot user and directories
# [ why this is complicated: https://github.com/moby/moby/issues/36677 ]
RUN mkdir -p /home/scot
RUN groupadd -g 1001 scot && \
    useradd -r -u 1001 -g scot scot
RUN chown scot:scot /home/scot /home/scot
USER scot
