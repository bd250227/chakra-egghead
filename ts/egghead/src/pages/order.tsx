import { FC } from "react";
import { Flex } from "@chakra-ui/react";
import { Details } from "../components/details";
import { Cart } from "../components/cart";

export const Order: FC = () => {
  return (
    <Flex h="100vh" py={20}>
      <Details />
      <Cart />
    </Flex>
  );
};
