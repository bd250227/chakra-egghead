import { FC } from "react";
import { Container } from "@chakra-ui/react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Home } from "./home";
import { Order } from "./order";

export const Pages: FC = () => {
  return (
    <Container maxW="container.xl" p={0}>
      <Router>
        <Switch>
          <Route path="/orders">
            <Order />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </Container>
  );
};
