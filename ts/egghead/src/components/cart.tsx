import { FC } from 'react';
import { Box, Heading, HStack, Image, Text, VStack } from '@chakra-ui/react';

export const Cart: FC = () => {
  return (
    <VStack
      w="full"
      h="full"
      p={10}
      spacing={10}
      alignItems="flex-start"
      bg="gray.50"
    >
      <VStack spacing={3} alignItems="flex-start">
        <Heading size="2xl">Your cart</Heading>
        <Text>If price is too hard on your eyes, try changing the theme</Text>
      </VStack>
      <HStack justifyContent="space-between" w="full">
        <HStack spacing={4} alignItems="center">
          <Image src="mtb.jpeg" h="96px" w="96px" />
          <VStack spacing={0} alignItems="flex-start">
            <Heading size="md">Penny board</Heading>
            <Text>PNYCOMP27541</Text>
          </VStack>
        </HStack>
        <Heading size="sm">$119.00</Heading>
      </HStack>
      <VStack spacing={2} w="full">
        <HStack justifyContent="space-between" w="full">
          <Text>Subtotal</Text>
          <Heading size="sm">$119.00</Heading>
        </HStack>
        <HStack justifyContent="space-between" w="full">
          <Text>Shipping</Text>
          <Heading size="sm">$19.99</Heading>
        </HStack>
        <HStack justifyContent="space-between" w="full">
          <Text>Taxes (estimated)</Text>
          <Heading size="sm">$23.80</Heading>
        </HStack>
      </VStack>
      <Box borderTopWidth={2} borderTopColor="gray.100" paddingTop={4} w="full">
        <HStack justifyContent="space-between" w="full">
          <Text>Total</Text>
          <Heading size="lg">$162.79</Heading>
        </HStack>
      </Box>
    </VStack>
  );
}